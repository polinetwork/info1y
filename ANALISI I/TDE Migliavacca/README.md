Starting from 2021, the professor is no longer teaching "Analisi e Geometria 1" for Ing. Gestionale but
"Analisi Matematica 1" for Ing. Info/Aut/Elett, so the exam format is a bit different.