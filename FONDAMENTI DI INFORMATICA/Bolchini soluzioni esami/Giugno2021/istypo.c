#include <stdio.h>
#include <string.h>
#define LEN 35
int istypo(char voc[], char dig[]){
	int i, j, len1, len2, cont;
	
	len1=strlen(voc);
	len2=strlen(dig);
	
	if(len2<len1){
		return 0;               /*se la parola digitata � pi� corta del mio vocabolo, chiudo subito il programma*/
	}
	
	i=0;
	j=0;
	cont=0;
	while(i<len1 && j<len2){
		if(voc[i]==dig[j]){
			cont++;
			i++;                  /*se trovo il carattere scorro entrambe le stringhe e aumento il cont...*/
			j++;
		}else{
			j++;              /*...altrimenti scorro solo dig fino a quando non trovo un elemento comune*/
		}
	if(cont==len1){          /*non appena trovo tutti i vocaboli di voc, chiudo il programm e restituisco 1*/
		return 1;
	}
}
return 0;               /* se il programma non entra nell' if precedente restituisco 0*/
}

int main(int argc, char*argv[]){
	char *vocabolo, *sorg;                  /*parametri acquisiti da riga di comando passati come puntatori*/
	char parola[LEN+1];    
	int cont=0;                
	
	if(argc==3){                                   /*fare il check degli elementi*/
		vocabolo=argv[1];
		sorg=argv[2];                             /*ricorda che argv[0] � occupato sempre dal nome del programma*/
	
	if(fp=fopen(sorg, "r")){
		while(fscanf(fp, "%s", parola)!=EOF){
			if(istypo(parola, voabolo)){
				printf("%s", parola);
				cont++;
			}
		}
		printf("%d", cont);
	fclose(fp);                 /*fclose...ricorda*/
    }else{
	printf("errore apertura file");
    }                                           /*ricordarsi di stampare i possibili errori*/
}else{
	printf("errore passaggio parametri");
}
return 0;
}          
