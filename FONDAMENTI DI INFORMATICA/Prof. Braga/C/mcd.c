//

#include <stdio.h>

int max (int a, int b){
    
    int massimo;
    
    if(a>b)
        massimo=a;
    else
        massimo=b;
        
    return massimo;
    
}

int main() {

    int num_1,num_2,mcd =0;
    
    printf("Inserire due numeri e calcolero' l'M.C.D. : ");
    scanf("%d%d",&num_1,&num_2);
    
    for(int i=1;i<=max(num_1, num_2);i++){
        
        if(num_1%i==0 && num_2%i==0 && (num_1!=0&&num_2!=0))
            
            mcd=i;

    }
    
    printf("l'M.C.D. tra %d e %d e': %d\n",num_1,num_2,mcd);
        
    return 0;

}
