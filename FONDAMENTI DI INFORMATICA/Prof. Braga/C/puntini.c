#include <stdio.h>

void punt(int n)
{
   
    if(n < 1000)
    {
        printf("%d",n);
    }
    
    else
    {
    
        if(n / 1000 < 1000)
        {
            printf("%d.",n / 1000);
        }
        
        else
        {
            punt(n / 1000);
            printf(".");
        }
        
        punt(n % 1000);
        
    }
    
    printf("\n");
   
}

int main(void)
{

	int num;
	
	printf("Insert number: ");
	scanf("%d", &num);
	
	punt(num);
	
}