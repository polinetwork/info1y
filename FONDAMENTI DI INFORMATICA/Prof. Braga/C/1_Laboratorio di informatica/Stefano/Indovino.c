#include <stdio.h>
#include <math.h>

#define MIN 0
#define MAX 1000

int main(){
	
	int guess=0, scommessa=0, min=MIN, max=MAX;
	char risposta=0;
	
	printf("---Indovino---\n");
	printf("Provero' ad indovinare un numero tra %d e %d\n", min, max);
	
	do{
		scommessa=(min+max)/2;
		printf("E' il %d ?", scommessa);
		scanf("%c", &risposta);
		fflush(stdin);
		if(risposta=='h'){
			max=scommessa-1;
			printf("Riprovo. ");
		}
		else if(risposta=='l'){
			min=scommessa+1;
			printf("Riprovo. ");
		}
	}while(risposta!='s');
	
	printf("Perfetto!!");
	
	return 0;
}
