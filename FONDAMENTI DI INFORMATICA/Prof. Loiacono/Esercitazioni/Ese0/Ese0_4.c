/*
	Esercizio 4:
	Scrivere un programma che, dato un numero interno che rappresenta un anno, verifica che l'anno inserito sia bisestile
	Nota: un anno è bisestile se è divisibile per 4, con la sola eccezione che gli anni secolari (ovvero gli anni divisibili per 100) sono bisestili solo se divisibili per 400
	--> E' bisestile se
			- l'anno è NON SECOLARE ed è divisibile per 4
			- l'anno è SECOLARE ed è divisibile per 400

*/

#include <stdio.h>
int main ()
{
	int anno;
	printf("\nInserisci anno: ");
	scanf("%d", &anno);

	if(anno%4 == 0 && anno%100 != 0 || anno%400 == 0)
		printf("L'anno %d è bisestile\n", anno);
	else
		printf("L'anno %d non è bisestile.\n", anno);
	return 0;
}
