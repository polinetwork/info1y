#include "liste.h"
#include <time.h>

int main()
{
  lista l=NULL;
  int  i,n,ric;
  srand(time(NULL));
  printf("Che versione vuoi testare? (0 iterativa, 1 ricorsiva) ");
  scanf("%d%*c",&ric);
  n = rand()%10 +1;
  for (i=0; i<n; i++)
  {
    int x = rand()%10;
    if (rand()%2>0)
    {
      printf("Inserisci in testa %d\n",x);
      l = inserisci_in_testa(l,x);
    }
    else
    {
      printf("Inserisci in coda %d\n",x);
      l = inserisci_in_coda(l,x,ric);
    }
    printf("Lista (l=%d): ",lunghezza(l,ric));
    stampa(l,ric);
  }

  for (i=0; i<n; i++)
  {
    int x = rand()%10;
    printf("Ricerca di %d: ",x);
    if (ricerca(l,x,ric)==NULL)
      printf("non trovato\n");
    else
      printf("trovato\n");
  }
  for (i=0; i<n+n/2; i++)
  {
    int choice = rand()%3;
    if (choice==0)
    {
      printf("Rimuovi in testa\n");
      l = rimuovi_in_testa(l);
    }
    else if (choice==1)
    {
      printf("Rimuovi in coda\n");
      l = rimuovi_in_coda(l,ric);
    }
    else
    {
      int x = rand()%10;
      printf("Rimuovi %d\n",x);
      l = rimuovi(l,x,ric);
    }
    printf("Lista (l=%d): ",lunghezza(l,ric));
    stampa(l,ric);
  }

  return 0;
}
