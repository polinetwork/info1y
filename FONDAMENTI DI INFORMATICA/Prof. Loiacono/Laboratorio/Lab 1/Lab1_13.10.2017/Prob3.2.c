#include <stdio.h>
#include <math.h> // fabs
#include <stdlib.h> // srand, rand
#include <time.h>// time


int main()
{
    float a, b, c;
    float vx, vy, x, y, TOL;

    srand(time(NULL)); //serve per far generare numeri diversi ogni volta alla funzione rand

    a = rand()%5 + 1; //genera un numero fra 1 e 5
    b = rand()%6; //genera un numero fra 0 e 5
    c = rand()%6; //genera un numero fra 0 e 5

    printf("a: %f\nb: %f\nc: %f\n", a, b, c);

    x = -b/(2*a);
    y = a*pow(x,2)+b*x+c; //pow(x,2) eleva x al quadrato
    //printf("X = %f\nY = %f\n", x, y);

    printf("Calcolare le coordinate del verice della parabola y=ax^2+bx+c e inserirle per valutarne la correttezza:\n");
    scanf("%f%f%*c", &vx, &vy);

    TOL = 0.1; //tolleranza per confrontare due float

    if(fabs(x-vx)<TOL)
        printf("Coordinata x del vertice CORRETTA\n");
    else
        printf("Coordinata x del vertice INCORRETTA\n");

    if(fabs(y-vy)<TOL)
        printf("Coordinata y del vertice CORRETTA\n");
    else
        printf("Coordinata y del vertice INCORRETTA\n");

    return 0;

}
